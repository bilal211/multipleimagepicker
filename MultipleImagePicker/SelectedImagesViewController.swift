//
//  SelectedImagesViewController.swift
//  MultipleImagePicker
//
//  Created by Bilal on 09/01/2018.
//  Copyright © 2018 ContentArcade. All rights reserved.
//

import UIKit
import TLPhotoPicker
import Photos

class SelectedImagesViewController: UIViewController {
    var assetsCollection = [TLPHAsset]()
    
    @IBOutlet weak var imageCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        printcounts()
    }
    
    
    func printcounts(){
        print("The total numbers of item in collection is, ", assetsCollection.count)
    }
    
   

 

}

extension SelectedImagesViewController : UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return assetsCollection.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imageCell", for: indexPath) as? imageCell
        
       var image = assetsCollection[indexPath.row].fullResolutionImage
        
        cell?.image.image = image
        return cell!
        
    }
    
    
}
