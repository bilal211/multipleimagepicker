//
//  imageCell.swift
//  MultipleImagePicker
//
//  Created by Bilal on 09/01/2018.
//  Copyright © 2018 ContentArcade. All rights reserved.
//

import UIKit

class imageCell: UICollectionViewCell {
    
    @IBOutlet weak var image: UIImageView!
}
