//
//  ViewController.swift
//  MultipleImagePicker
//
//  Created by Bilal on 09/01/2018.
//  Copyright © 2018 ContentArcade. All rights reserved.
//

import UIKit
import TLPhotoPicker
import Photos

class ViewController: UIViewController, TLPhotosPickerViewControllerDelegate {

    
      var selectedAssets = [TLPHAsset]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func pickrBtnPressed(_ sender: Any) {
        let viewController = TLPhotosPickerViewController()
        viewController.delegate = self
        viewController.didExceedMaximumNumberOfSelection = { [weak self] (picker) in
            self?.showAlert(vc: picker)
        }
        var configure = TLPhotosPickerConfigure()
        configure.numberOfColumn = 3
        configure.maxSelectedAssets = 10
        configure.allowedLivePhotos = true
        viewController.configure = configure
        viewController.selectedAssets = self.selectedAssets
        
        self.present(viewController, animated: true, completion: nil)
    
    }
    
    func dismissPhotoPicker(withTLPHAssets: [TLPHAsset]) {
        // use selected order, fullresolution image
        self.selectedAssets = withTLPHAssets
       
    }
    func dismissComplete() {
        performSegue(withIdentifier: "toImage", sender: nil)
    }
    
    
    func showAlert(vc: UIViewController) {
        let alert = UIAlertController(title: "", message: "Exceed Maximum Number Of Selection", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        vc.present(alert, animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? SelectedImagesViewController {
            print("in vc.....waooo")
            vc.assetsCollection = selectedAssets
        }
    }
    
    
    
}

